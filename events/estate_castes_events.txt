namespace = estate_castes_events

country_event = {
	id = estate_castes_events.1
	title = estate_castes_events.1.t
	desc = estate_castes_events.1.d
	picture = BURGHER_ESTATE_eventPicture
	
	trigger = {
		has_estate = estate_uppercastes
	}

	is_triggered_only = yes

	option = { # What do you mean 'garbage'? This is hilarious! Invite him to court!
		name = estate_castes_events.1.a
		define_advisor = {
			type = artist
			name = "Dhanneh Leihner"
			culture = rabhidarubsad
			skill = 2
			discount = yes
		}
		add_estate_loyalty_modifier = {
			estate = estate_uppercastes
			desc = EST_VAL_CASTES_RIGID_DISLOYALTY
			loyalty = -10
			duration = 5475
		}
		add_estate_loyalty_modifier = {
			estate = estate_middlecastes
			desc = EST_VAL_CASTES_FLUID_LOYALTY
			loyalty = 5
			duration = 5475
		}
		add_estate_loyalty_modifier = {
			estate = estate_lowercastes
			desc = EST_VAL_CASTES_FLUID_LOYALTY
			loyalty = 5
			duration = 5475
		}
	}
	option = { # If we ignore him, his works will soon be forgotten.
		name = estate_castes_events.1.b #Ok
		add_estate_influence_modifier = {
			estate = estate_burghers
			desc = EST_VAL_BURGHERS_ADVANCE
			influence = 10
			duration = 5475
		}
	}
	option = { # Hunt him down and make him pay for this insolence!
		name = estate_castes_events.1.c #Ok
		add_estate_influence_modifier = {
			estate = estate_burghers
			desc = EST_VAL_BURGHERS_ADVANCE
			influence = 10
			duration = 5475
		}
	}
}


country_event = {
	id = estate_castes_events.2
	title = estate_castes_events.2.t
	picture = BURGHER_ESTATE_eventPicture
	
	trigger = {
		has_estate = estate_middlecastes
	}
	desc = { trigger = { has_country_flag = choose_harimari_in_uppercastes }  desc = estate_castes_events.2.da }
	desc = { trigger = { has_country_flag = choose_human_in_uppercastes } desc = estate_castes_events.2.db }
	desc = { trigger = { has_country_flag = choose_harimari_in_lowercastes }  desc = estate_castes_events.2.dc }
	desc = { trigger = { has_country_flag = choose_human_in_lowercastes } desc = estate_castes_events.2.dd }

	is_triggered_only = yes

	immediate = {
		hidden_effect = {
			random_list = {
				10 = {
					set_country_flag = choose_harimari_in_uppercastes
					modifier = { 
						factor = 0
						NOT = { has_country_flag = harimari_in_uppercastes }
					}
					modifier = { 
						factor = 0.5
						has_country_flag = human_in_uppercastes
					}
				}
				10 = {
					set_country_flag = choose_human_in_uppercastes
					modifier = {
						factor = 0
						NOT = { has_country_flag = human_in_uppercastes }
					}
					modifier = { 
						factor = 0.5
						has_country_flag = harimari_in_uppercastes
					}
				}
				10 = {
					set_country_flag = choose_harimari_in_lowercastes
					modifier = {
						factor = 0
						NOT = { has_country_flag = harimari_in_lowercastes }
					}
					modifier = { 
						factor = 0.5
						has_country_flag = human_in_lowercastes
					}
				}
				10 = {
					set_country_flag = choose_human_in_lowercastes
					modifier = {
						factor = 0
						NOT = { has_country_flag = human_in_lowercastes }
					}
					modifier = { 
						factor = 0.5
						has_country_flag = harimari_in_lowercastes
					}
				}
			}
		}
	}
	after = {
		hidden_effect = {
			clr_country_flag = choose_harimari_in_uppercastes
			clr_country_flag = choose_human_in_uppercastes
			clr_country_flag = choose_harimari_in_lowercastes
			clr_country_flag = choose_human_in_lowercastes
		}
	}

	option = { # What do you mean 'garbage'? This is hilarious! Invite him to court!
		name = estate_castes_events.2.a
		define_advisor = {
			type = artist
			name = "Dhanneh Leihner"
			culture = rabhidarubsad
			skill = 2
			discount = yes
		}
	}
	option = { # If we ignore him, his works will soon be forgotten.
		name = estate_castes_events.2.b #Ok
		add_prestige = 10
	}
	option = { # Hunt him down and make him pay for this insolence!
		name = estate_castes_events.2.c #Ok
		add_prestige = 10
	}
}


country_event = {
	id = estate_castes_events.100
	title = estate_castes_events.100.t
	desc = estate_castes_events.100.desc
	picture = 9_Vaisya_Estate_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		tag = R52
		has_country_flag = has_caste_estates
	}

	is_triggered_only = yes

	option = { # descrease fluidity small
		name = estate_castes_events.100.a
		custom_tooltip = caste_fluidity_decrease_small_tt
		hidden_event = { caste_fluidity_decrease_small = yes }
	}
	option = { # descrease fluidity medium
		name = estate_castes_events.100.b
		custom_tooltip = caste_fluidity_decrease_medium_tt
		hidden_event = { caste_fluidity_decrease_medium = yes }
	}
	option = { # descrease fluidity large
		name = estate_castes_events.100.b
		custom_tooltip = caste_fluidity_decrease_large_tt
		hidden_event = { caste_fluidity_decrease_large = yes }
	}
	option = { # keep fluidity equal
		name = estate_castes_events.100.c
		custom_tooltip = caste_fluidity_neutral_tt
		# no change
	}
	option = { # increase fluidity small
		name = estate_castes_events.100.d
		custom_tooltip = caste_fluidity_increase_small_tt
		hidden_event = { caste_fluidity_increase_small = yes }
	}
	option = { # increase fluidity medium
		name = estate_castes_events.100.e
		custom_tooltip = caste_fluidity_increase_medium_tt
		hidden_event = { caste_fluidity_increase_medium = yes }
	}
	option = { # increase fluidity large
		name = estate_castes_events.100.e
		custom_tooltip = caste_fluidity_increase_large_tt
		hidden_event = { caste_fluidity_increase_large = yes }
	}
}